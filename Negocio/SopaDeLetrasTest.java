package Negocio;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaDeLetrasTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetrasTest
{
    /**
     * Default constructor for test class SopaDeLetrasTest
     */
    public SopaDeLetrasTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    { 

    }

    @Test
    public void crearMatriz() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void crearMatriz_conError() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras();

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void esCuadrada() throws Exception
    {
        String palabras="juan,jose,luis,pepe";
        SopaDeLetras c=new SopaDeLetras(palabras);
        boolean pasoPrueba = c.esCuadrada();
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void esCuadrada2() throws Exception
    {
        String palabras="a";
        SopaDeLetras c=new SopaDeLetras(palabras);
        boolean pasoPrueba = c.esCuadrada();
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void esCuadrada3() throws Exception
    {
        String palabras="la,el";
        SopaDeLetras c=new SopaDeLetras(palabras);
        boolean pasoPrueba = c.esCuadrada();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void esCuadrada_conError() throws Exception
    {
        String palabras="juanito,joselo,luisa,pepe";
        SopaDeLetras c=new SopaDeLetras(palabras);
        boolean pasoPrueba = c.esCuadrada();
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void esCuadrada_conError2() throws Exception
    {
        String palabras="ab";
        SopaDeLetras c=new SopaDeLetras(palabras);
        boolean pasoPrueba = c.esCuadrada();
        assertEquals(false,pasoPrueba);
    }

    @Test
    public void esDispersa() throws Exception
    {
        String palabras="jose,mariana,luis,tu";
        SopaDeLetras d=new SopaDeLetras(palabras);
        boolean pasoPrueba = d.esDispersa();
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void esDispersa2() throws Exception
    {
        String palabras="lee,mariana,a,paralelepipedo";
        SopaDeLetras d=new SopaDeLetras(palabras);
        boolean pasoPrueba = d.esDispersa();
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void esDispersa3() throws Exception
    {
        String palabras="lee,a,";
        SopaDeLetras d=new SopaDeLetras(palabras);
        boolean pasoPrueba = d.esDispersa();
        assertEquals(true,pasoPrueba);
    }

    @Test
    public void esDispersa_conError() throws Exception
    {
        String palabras="marcos,joselo,julian,alfred";
        SopaDeLetras d=new SopaDeLetras(palabras);
        boolean pasoPrueba = d.esDispersa();
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void esDispersa_conError2() throws Exception
    {
        String palabras="juan,jose,luis,pepe";
        SopaDeLetras d=new SopaDeLetras(palabras);
        boolean pasoPrueba = d.esDispersa();
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void esRectancular() throws Exception
    {
        String palabras="marcos,joselo,albert,pepito";
        SopaDeLetras r=new SopaDeLetras(palabras);
        boolean pasoPrueba = r.esRectangular();
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void esRectancular2() throws Exception
    {
        String palabras="lu,to,el,ma";
        SopaDeLetras r=new SopaDeLetras(palabras);
        boolean pasoPrueba = r.esRectangular();
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void esRectancular3() throws Exception
    {
        String palabras="lucia,tomas";
        SopaDeLetras r=new SopaDeLetras(palabras);
        boolean pasoPrueba = r.esRectangular();
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void esRectancular_conError() throws Exception
    {
        String palabras="marc,joselo,alb,pe";
        SopaDeLetras r=new SopaDeLetras(palabras);
        boolean pasoPrueba = r.esRectangular();
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void esRectancular_conError2() throws Exception
    {
        String palabras="lua,ana,alb";
        SopaDeLetras r=new SopaDeLetras(palabras);
        boolean pasoPrueba = r.esRectangular();
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void contarPalabras() throws Exception
    {
        String palabras="lua,ana,alb";
        SopaDeLetras r=new SopaDeLetras(palabras);       
        Integer cantidad = 4;
        boolean pasoPrueba = false;
        if(r.getContar("a") == cantidad) pasoPrueba = true;
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void contarPalabras2() throws Exception
    {
        String palabras="marco,polo,lucia";
        SopaDeLetras r=new SopaDeLetras(palabras);        
        Integer cantidad = 1;
        boolean pasoPrueba = false;
        if(r.getContar("lucia") == cantidad) pasoPrueba = true;
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void contarPalabras3() throws Exception
    {
        String palabras="marco,polo,lucia,matias,lucia,juan,pepe,lucia,alberto";
        SopaDeLetras r=new SopaDeLetras(palabras);        
        Integer cantidad = 3;
        boolean pasoPrueba = false;
        if(r.getContar("lucia") == cantidad) pasoPrueba = true;
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void contarPalabras_conError() throws Exception
    {
        String palabras="lua,ana,alb";
        SopaDeLetras r=new SopaDeLetras(palabras);        
        Integer cantidad = 4;
        boolean pasoPrueba = false;
        if(r.getContar("e") == cantidad) pasoPrueba = true;
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void contarPalabras_conError2() throws Exception
    {
        String palabras="marcos,pepe,tomas,juan,leemar";
        SopaDeLetras r=new SopaDeLetras(palabras); 
        Integer cantidad = 1;
        boolean pasoPrueba = false;
        if(r.getContar("tomasjuan") == cantidad) pasoPrueba = true;
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void diagonalPrincipal() throws Exception        
    {
        String palabras="aed,ebd,edc";
        SopaDeLetras di=new SopaDeLetras(palabras);
        boolean pasoPrueba = false;
        if(di.esCuadrada() == true)
        {
            char ideal []= {'a','b','c'};
            if(sonIgualesVector(ideal,di.getDiagonalPrincipal()))pasoPrueba = true;
        }
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void diagonalPrincipal2() throws Exception        
    {
        String palabras="juan,paco,jose,tomi";
        SopaDeLetras di=new SopaDeLetras(palabras);
        boolean pasoPrueba = false;
        if(di.esCuadrada() == true)
        {
            char ideal []= {'j','a','s','i'};
            if(sonIgualesVector(ideal,di.getDiagonalPrincipal()))pasoPrueba = true;
        }
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void diagonalPrincipal3() throws Exception        
    {
        String palabras="z";
        SopaDeLetras di=new SopaDeLetras(palabras);
        boolean pasoPrueba = false;
        if(di.esCuadrada() == true)
        {
            char ideal []= {'z'};
            if(sonIgualesVector(ideal,di.getDiagonalPrincipal()))pasoPrueba = true;
        }
        assertEquals(true,pasoPrueba);
    }
    
    @Test
    public void diagonalPrincipal_conError() throws Exception        
    {
        String palabras="aed,ebd,edc";
        SopaDeLetras di=new SopaDeLetras(palabras);
        boolean pasoPrueba = false;
        if(di.esCuadrada() == true)
        {
            char ideal []= {'c','b','a'};
            if(sonIgualesVector(ideal,di.getDiagonalPrincipal()))pasoPrueba = true;
        }
        assertEquals(false,pasoPrueba);
    }
    
    @Test
    public void diagonalPrincipal_conError2() throws Exception        
    {
        String palabras="tomas,juan,alberto";
        SopaDeLetras di=new SopaDeLetras(palabras);
        boolean pasoPrueba = false;        
        if(di.esCuadrada() == true)
        {
            char ideal []= {'n'};
            if(sonIgualesVector(ideal,di.getDiagonalPrincipal()))pasoPrueba = true;
        }
        assertEquals(false,pasoPrueba);
    }

    private boolean sonIguales(char m1[][], char m2[][])
    {

        if(m1==null || m2==null || m1.length !=m2.length)
            return false; 

        int cantFilas=m1.length; 

        for(int i=0;i<cantFilas;i++)
        {
            if( !sonIgualesVector(m1[i],m2[i]))
                return false;
        }

        return true;
    }

    private boolean sonIgualesVector(char v1[], char v2[])
    {
        if(v1.length != v2.length)
            return false;

        for(int j=0;j<v1.length;j++)
        {
            if(v1[j]!=v2[j])
                return false;
        }
        return true;
    }
}

